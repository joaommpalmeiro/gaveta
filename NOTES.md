# Notes

- https://github.com/joaopalmeiro/template-python-package
- https://codeberg.org/joaopalmeiro/cblone/src/branch/main/NOTES.md
- https://github.com/mahmoud/boltons
- https://breadcrumbscollector.tech/where-to-put-all-your-utils-in-python-projects/
- https://docs.djangoproject.com/en/5.0/ref/utils/
- https://github.com/django/django/tree/5.0/django/utils
- https://docs.djangoproject.com/en/5.0/topics/serialization/#django.core.serializers.json.DjangoJSONEncoder
- https://github.com/django/django/blob/5.0/django/core/serializers/json.py
- [RET503 doesn't respect NoReturn](https://github.com/astral-sh/ruff/issues/5474) issue
  - https://github.com/astral-sh/ruff/commit/45628a58831628441d592e00b35470401fde0592
- https://github.com/sg495/typing-json
- https://github.com/python/typing/issues/182#issuecomment-1320974824
- https://github.com/python/mypy/issues/13786#issuecomment-1320481633: "Python's own builtin `json` module, for example, insists on `list`s and `dict`s (or subclasses thereof) and will fail if you e.g. give it another kind of mapping:"
- https://github.com/v7labs/benchllm/blob/v0.3.0/benchllm/input_types.py: `Json = Union[str, bool, list, dict]`
- https://github.com/python/typeshed/blob/e80ad6b2bce7ef6b2a20aec2d79a672859b31864/stubs/requests/requests/models.pyi#L150
- https://github.com/great-expectations/great_expectations/blob/0.18.10/great_expectations/alias_types.py: `PathStr: TypeAlias = Union[str, pathlib.Path]`
- https://docs.python.org/3/library/typing.html#typing.TypeAlias
- https://github.com/kevinheavey/jsonalias:
  - https://github.com/kevinheavey/jsonalias/issues/2
  - https://github.com/kevinheavey/jsonalias/blob/2a98db9c4024b1914f198f088f2aaa15a8cd7423/jsonalias/__init__.py: `Json = Union[Dict[str, "Json"], List["Json"], str, int, float, bool, None]`
- https://github.com/python/cpython/blob/3.10/Lib/json/__init__.py#L120
- https://github.com/python/typeshed/blob/e80ad6b2bce7ef6b2a20aec2d79a672859b31864/stdlib/json/__init__.pyi#L24: `def dump(obj: Any, ...)`
- https://hatch.pypa.io/latest/config/build/:
  - "Although not recommended, you may define global configuration in the `tool.hatch.build` table. Keys may then be overridden by target config."
  - https://hatch.pypa.io/latest/config/build/#explicit-selection: `only-include = ["src"]` or `packages = ["src/gaveta"]`
  - https://github.com/FlorianWilhelm/the-hatchlor/blob/v0.3/%7B%7Bcookiecutter.project_slug%7D%7D/pyproject.toml#L69
- https://github.com/pypa/hatch/issues/561
  - `HATCH_INDEX_USER` + `HATCH_INDEX_AUTH` environment variables
  - https://hatch.pypa.io/latest/publish/#authentication
  - https://github.com/jaraco/keyring
  - https://github.com/pypa/hatch/blob/hatch-v1.9.3/src/hatch/publish/index.py#L56:
    - Name: `main`
- https://blog.whtsky.me/tech/2021/dont-forget-py.typed-for-your-typed-python-package/
- https://mypy.readthedocs.io/en/stable/installed_packages.html#creating-pep-561-compatible-packages
- https://github.com/pypi/warehouse/issues/4348
- https://github.com/python-pillow/Pillow/pull/7822/files
- https://typing.readthedocs.io/en/latest/reference/best_practices.html#using-any-and-object:
  - "Generally, use `Any` when a type cannot be expressed appropriately with the current type system or using the correct type is unergonomic."
  - https://docs.python.org/3.10/library/typing.html#typing.Any
- https://adamj.eu/tech/2021/07/06/python-type-hints-how-to-use-typing-cast/
- https://stackoverflow.com/questions/9885217/in-python-if-i-return-inside-a-with-block-will-the-file-still-close
- `assert_never()`:
  - https://github.com/python/typing_extensions/blob/4.12.2/src/typing_extensions.py#L2569-L2595
  - https://docs.python.org/3/library/typing.html#typing.assert_never
  - https://github.com/astral-sh/ruff-lsp/blob/710c706ec2b3c4036827fc5fbed9aa0fde5dfbf1/ruff_lsp/server.py#L79
- Documentation:
  - https://squidfunk.github.io/mkdocs-material/alternatives/#sphinx
  - https://github.com/mkdocstrings/mkdocstrings
  - https://mkdocstrings.github.io/
  - https://mkdocstrings.github.io/python/
  - https://github.com/mkdocs/mkdocs
  - https://www.mkdocs.org/getting-started/
  - https://squidfunk.github.io/mkdocs-material/creating-your-site/#configuration
  - https://hatch.pypa.io/1.12/environment/#selection
  - https://mkdocstrings.github.io/python/usage/
  - https://github.com/mkdocstrings/mkdocstrings/blob/0.25.2/mkdocs.yml
  - https://mkdocstrings.github.io/python/usage/configuration/general/
  - https://pdm-project.org/en/latest/
  - https://github.com/pdm-project/pdm/blob/2.17.3/mkdocs.yml
  - https://www.mkdocs.org/user-guide/configuration/
  - https://squidfunk.github.io/mkdocs-material/setup/changing-the-colors/#automatic-light-dark-mode
  - https://github.com/squidfunk/mkdocs-material/blob/9.5.31/docs/getting-started.md?plain=1#L27-L55
  - https://mkdocstrings.github.io/python/usage/configuration/docstrings/#docstring_options:
    - https://numpydoc.readthedocs.io/en/latest/install.html#configuration
    - https://numpydoc.readthedocs.io/en/latest/format.html
    - https://mkdocstrings.github.io/griffe/reference/docstrings/#numpydoc-style
  - https://mkdocstrings.github.io/python/usage/customization/#material
  - https://mkdocstrings.github.io/python/usage/#options-summary:
    - "`separate_signature`: (...) If Black is installed, the signature is also formatted using it."
    - https://github.com/mkdocstrings/python/blob/1.10.7/src/mkdocstrings_handlers/python/rendering.py#L437-L452
  - [harmless deprecation warnings](https://github.com/mkdocstrings/mkdocstrings/issues/676) issue
- https://hatch.pypa.io/1.12/tutorials/environment/basic-usage/#using-the-default-environment:
  - "Hatch will always use the `default` environment if an environment is not chosen explicitly when running a command."
  - "You never need to manually create environments as spawning a shell or running commands within one will automatically trigger creation."
- https://hatch.pypa.io/1.12/tutorials/environment/basic-usage/#configure-the-default-environment
- https://www.npmjs.com/package/dpdm:
  - https://github.com/acrazing/dpdm
  - "A robust static dependency analyzer for your JavaScript and TypeScript projects."
  - "Detect circular dependencies in your TypeScript projects."
- https://github.com/antfu/eslint-config/blob/1215d9fd98e4b6fee57453312a22c2340c91f085/src/utils.ts#L105-L124
- https://www.npmjs.com/package/local-pkg
- https://dockx.app/
- https://www.andreagrandi.it/posts/kagi-paid-search-engine/
- https://gitlab.com/ing_rpaa/ing_theme_matplotlib
- https://github.com/mckinsey/qbstyles
- https://github.com/astral-sh/ruff/releases
- [`"Never" does not derive from BaseException` false positive when raising `Never`](https://github.com/microsoft/pyright/issues/6619):
  - "(...) then Ruff will trigger the following error because it doesn't have access to the type information:"
- GLab:
  - https://gitlab.com/gitlab-org/cli
  - https://gitlab.com/gitlab-org/cli/-/blob/e455e6f2c620e58d14e537e8f31e431d87a8563d/docs/source/release/create.md:
    - `-D, --released-at string The 'date' when the release was ready. Defaults to the current datetime. Expects ISO 8601 format (2019-03-15T08:00:00Z).`
  - https://gitlab.com/gitlab-org/cli/-/blob/main/README.md?ref_type=heads#authentication:
    - `` The minimum required scopes for the token are: `api`, `write_repository`. ``
  - https://vitejs.dev/config/preview-options.html#preview-open: `` you can set the env `process.env.BROWSER` (e.g. `firefox`) ``
  - https://github.com/sindresorhus/default-browser
  - https://github.com/sindresorhus/default-browser-cli
  - https://github.com/sindresorhus/default-browser-id
  - https://developer.apple.com/documentation/bundleresources/information_property_list/cfbundleidentifier
  - https://github.com/sindresorhus/open/tree/v10.1.0?tab=readme-ov-file#apps
  - https://github.com/sindresorhus/open/blob/v10.1.0/index.js#L328-L355
  - https://github.com/sindresorhus/open/blob/v10.1.0/index.js#L106-L123
  - https://kinsta.com/knowledgebase/what-is-an-environment-variable/: `VAR_UNO=SOMETHING node index.js`
  - https://github.com/RhetTbull/osxmetadata
  - https://developer.apple.com/documentation/bundleresources/information_property_list/cfbundleexecutable:
    - Firefox: `<key>CFBundleExecutable</key>` + `<string>firefox</string>`
  - https://gitlab.com/gitlab-org/cli/-/blob/e455e6f2c620e58d14e537e8f31e431d87a8563d/pkg/utils/utils.go#L17-24: `OpenInBrowser opens the url in a web browser based on OS and $BROWSER environment variable`
  - https://gitlab.com/gitlab-org/cli/-/blob/e455e6f2c620e58d14e537e8f31e431d87a8563d/pkg/browser/browser.go
- UUIDs:
  - https://www.unkey.com/blog/uuid-ux:
    - https://github.com/unkeyed/unkey
    - "While using a standard UUID will satisfy all your security concerns, there's a lot we can improve for our users."
    - UX:
      - "Make them easy to copy"
        - "This can be achieved by removing the hyphens from the UUIDs (...)"
      - "Prefixing"
      - "More efficient encoding"
      - "Changing the length"
    - "Base58 encoding uses a larger character set and avoids ambiguous characters, such as upper case `I` and lower case `l` resulting in shorter identifier strings without compromising readability."
    - https://github.com/ai/nanoid
    - https://github.com/unkeyed/unkey/blob/327ad793c8126a2e957d072e8d066bcad49fbf4e/internal/id/src/generate.ts
    - https://www.npmjs.com/package/base-x
  - https://docs.python.org/3.10/library/uuid.html:
    - "If all you want is a unique ID, you should probably call `uuid1()` or `uuid4()`."
    - "Note that `uuid1()` may compromise privacy since it creates a UUID containing the computer's network address. `uuid4()` creates a random UUID."
  - https://github.com/oliverlambson/fastnanoid
  - https://python-ulid.readthedocs.io/en/latest/
  - https://discuss.python.org/t/add-uuid7-in-uuid-module-in-standard-library/44390: "UUID7 has better performance and collision resistance than uuid4."
- https://github.com/python/mypy/issues/13755
- https://github.com/squidfunk/mkdocs-material/releases

## Commands

```bash
hatch run mkdocs new .
```

```bash
hatch env show --internal
```

```bash
hatch env prune
```

```bash
glab --version
```

```bash
npx default-browser-cli
```

```bash
defaults read com.apple.LaunchServices/com.apple.launchservices.secure LSHandlers
```

### Clean slate

```bash
rm -rf .hatch/ .mypy_cache/ .ruff_cache/ dist/ site/ src/gaveta/__pycache__/
```

## Snippets

```python
# Source:
# - Great Expectations (https://github.com/great-expectations/great_expectations/blob/0.18.10/great_expectations/alias_types.py)
# - Shantanu (https://github.com/python/typing/issues/182#issuecomment-1320974824)
# - jsonalias by Kevin Heavey (https://github.com/kevinheavey/jsonalias)
JSONValues: TypeAlias = (
    dict[str, "JSONValues"] | list["JSONValues"] | str | int | float | bool | None
)
```

### `ruff.toml` file

```toml
# https://docs.astral.sh/ruff/settings/#required-version
required-version = "0.3.0"
# https://docs.astral.sh/ruff/settings/#target-version
target-version = "py310"
# https://docs.astral.sh/ruff/settings/#output-format
output-format = "full"
# https://docs.astral.sh/ruff/settings/#preview
preview = false

[lint]
# All:
# https://docs.astral.sh/ruff/linter/#rule-selection
select = ["ALL"]
# https://docs.astral.sh/ruff/settings/#lint_fixable
fixable = ["ALL"]
# https://docs.astral.sh/ruff/settings/#lint_ignore
# https://docs.astral.sh/ruff/rules/#pydocstyle-d
# https://docs.astral.sh/ruff/formatter/#conflicting-lint-rules
ignore = ["D", "COM812", "ISC001"]

# Minimal:
# https://docs.astral.sh/ruff/settings/#lint_extend-select
# https://docs.astral.sh/ruff/rules/#isort-i
# extend-select = ["I"]

# https://docs.astral.sh/ruff/settings/#lint_isort_combine-as-imports
[lint.isort]
combine-as-imports = true
```
